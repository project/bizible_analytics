Bizible Analytics - http://bizible.com
=================
Adds Bizible's analytics script on non admin pages.

Installation
============
To add the analytics script simply enable the module, disable the module to
remove it. No additional configuration is needed. Login to bizible.com to review
analytics.
